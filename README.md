# ISO and Image Directory

This is a small Bootstrap and Boxicons based static landing page to get fast access to the download pages of the most
common operating systems. It can be either self-hosted or used
via [Codeberg Pages](https://wh0ami.codeberg.page/ISOandImageDirectory/public/).

### Hotkeys

| Key           | Function                     |
|---------------|------------------------------|
| f             | focus on search bar          |
| x             | clear search bar             |

### Licensing
My part of the source code is licensed under Public Domain (as mentioned in `LICENSE`), but Bootstrap and Boxicons and
also the linked projects/distributions and their logos have their own licenses.

### DISCLAIMER
I don't work on the linked projects and distributions, and I don't own them. I comply with the licensing
requirements regarding their logos to the best of my knowledge and belief. If you are missing your project or
distribution or if you don't want to see your distribution as a part of this page  (or if you have any other problem),
please create an issue. Thanks!
