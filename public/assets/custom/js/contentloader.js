function loadISO(searchstring) {
    fetch('content/iso.json')
        .then(response => response.json())
        .then(json => {
            /** transform data into cards **/
            let list = '';
            let card = '';
            let torrent = '';
            let link = '';

            json.sort(function (a, b) {
                let x = a.title.toLowerCase();
                let y = b.title.toLowerCase();
                if (x < y) {
                    return -1;
                }
                if (x > y) {
                    return 1;
                }
                return 0;
            });

            for (let entry in json) {
                let data = json[entry];

                if (data.title.toLowerCase().includes(searchstring.toLowerCase()) || searchstring === "") {
                    /* Top, Logo and Headline */
                    card = '<div class="col mb-4"><div class="card border-light text-white bg-dark h-100"><a class="text-white" href="' + data.website + '" rel="noopener,noreferrer"><img src="content/logo/' + data.image + '" class="card-img-top bg-light" alt="' + data.title + ' Logo" title="Click to open project website"></a><div class="card-body"><h5 class="card-title">' + data.title + '</h5><ul class="list-group list-group-flush"><li class="list-group-item bg-dark border-light"><i class="bx bxs-chip"></i>&nbsp;' + data.arch + '</li>';

                    /* Full Installer */
                    torrent = (data.full_torrent === '') ? '&nbsp;' : '<a class="text-white" href="' + data.full_torrent + '" rel="noopener,noreferrer"><i class="bx bxs-magnet"></i>&nbsp;via Torrent</a>';
                    card += '<li class="list-group-item bg-dark border-light"><a class="text-white" href="' + data.full + '" rel="noopener,noreferrer"><i class="bx bxs-disc"></i>&nbsp;Full ISO/Image</a><br />' + torrent + '</li>';

                    /* Net Installer and Bottom*/
                    link = (data.netinst === "") ? '<i class="bx bxs-network-chart"></i>&nbsp;<del>Net Installer</del>' : '<a class="text-white" href="' + data.netinst + '" rel="noopener,noreferrer"><i class="bx bxs-network-chart"></i>&nbsp;Net Installer</a>';
                    torrent = (data.netinst_torrent === '') ? '&nbsp;' : '<a class="text-white" href="' + data.netinst_torrent + '" rel="noopener,noreferrer"><i class="bx bxs-magnet"></i>&nbsp;via Torrent</a>';
                    card += '<li class="list-group-item bg-dark">' + link + '<br />' + torrent + '</li></ul></div></div></div>';


                    /* append to list */
                    list += card;
                }
            }

            /* catch empty answer */
            if (list === "") {
                list = '<h1 class="text-light"><br />&emsp;No results.</h1>';
            }

            document.getElementById('isolist').innerHTML = list;
        });
}

/** function for fast getting an element **/
function getElement(id) {
    return document.getElementById(id);
}

/** function for writing a message to the browser log/console **/
function writeLog(msg) {
    console.log('[ISOandImageDirectory] ' + msg);
}

/* EventListener for searchbar */
document.getElementById("searchbar").addEventListener('input', e => {
    loadISO(e.target.value);
});

/* EventListener for clearbutton of searchbar */
document.getElementById("clearbutton").addEventListener('click', () => {
    document.getElementById("searchbar").value = "";
    loadISO("");
});

/* initial loading of ISOs with page loading */
document.getElementById("searchbar").value = "";
loadISO('');

/** hotkeys - will be only triggered, if the user is not focussing the searchbar **/
document.addEventListener('keyup', (e) => {
    if (document.activeElement.id !== 'searchbar') {
        switch (e.key) {
            case 'f':
                writeLog('Hotkey "f" pressed - focussing on searchbar');
                getElement('searchbar').focus();
                break;
            case 'x':
                writeLog('Hotkey "x" pressed - clearing the searchbar');
                getElement('clearbutton').dispatchEvent(new Event('click'));
                break;
            default:
                break;
        }
    }
});
